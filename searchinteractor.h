#ifndef SEARCHINTERACTOR_H
#define SEARCHINTERACTOR_H

#include <QObject>
#include <QPushButton>
#include <QListWidget>
#include <QComboBox>
#include <QFileDialog>
#include <QDir>
#include <QLineEdit>
#include <QRegExp>
#include <QStringList>
#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include <QStatusBar>
#include <QProgressDialog>
#include <QCoreApplication>


class SearchInteractor : public QObject
{
    Q_OBJECT
    QPushButton* btnSearch;
    QPushButton* btnErase;
    QListWidget* list;
    QComboBox* searchDir;
    QLineEdit* inpRegex;
    QStatusBar* status;
public:
    SearchInteractor(QObject *parent = nullptr, QPushButton* btnSearch = nullptr,
                     QPushButton* btnErase  = nullptr, QListWidget* list = nullptr,
                     QComboBox* searchDir = nullptr, QLineEdit *inpRegex = nullptr,
                     QStatusBar *status = nullptr);

    void findRecursive(const QString &path, QStringList *result, QProgressDialog &progress);
signals:

public slots:
    void search();
    void erase();
    void listItemDoubleClicked(QListWidgetItem* item);
};

#endif // SEARCHINTERACTOR_H
