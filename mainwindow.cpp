#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    interactor = new SearchInteractor(this, ui->btnSearch, ui->btnErase, ui->itemList,
                                      ui->searchDir, ui->inpRegex, ui->statusBar);
    help = new HelpDialog(this);
    about = new About(this);
    ui->statusBar->showMessage(tr("Очікую введення"));

    connect(ui->actionExit, &QAction::triggered, this, &QMainWindow::close);
    connect(ui->btnBrowse, &QPushButton::clicked, this, &MainWindow::browse);
    connect(ui->menuHelp, &QMenu::aboutToShow, help, &HelpDialog::show);
    connect(ui->menu, &QMenu::aboutToShow, about, &About::show);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete interactor;
    delete help;
    delete about;
}

void MainWindow::browse()
{
    QString directory =
            QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this, tr("Знайти"), QDir::currentPath()));

    if (!directory.isEmpty()) {
        if (ui->searchDir->findText(directory) == -1)
            ui->searchDir->addItem(directory);
        ui->searchDir->setCurrentIndex(ui->searchDir->findText(directory));
    }
}
