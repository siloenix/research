#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "searchinteractor.h"
#include "helpdialog.h"
#include "about.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    SearchInteractor* interactor;
    HelpDialog* help;
    About* about;

public slots:
    void browse();
};

#endif // MAINWINDOW_H
