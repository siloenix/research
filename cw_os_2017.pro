#-------------------------------------------------
#
# Project created by QtCreator 2017-12-08T06:34:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cw_os_2017
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    searchinteractor.cpp \
    helpdialog.cpp \
    about.cpp

HEADERS  += mainwindow.h \
    searchinteractor.h \
    helpdialog.h \
    about.h

FORMS    += mainwindow.ui \
    helpdialog.ui \
    about.ui

DISTFILES += \
    .gitignore

RESOURCES += \
    resources.qrc
