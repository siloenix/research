#include "searchinteractor.h"

SearchInteractor::SearchInteractor(QObject *parent, QPushButton *btnSearch, QPushButton *btnErase, QListWidget *list,
                                   QComboBox* searchDir, QLineEdit* inpRegex, QStatusBar* status)
{
    this->btnErase = btnErase;
    this->btnSearch = btnSearch;
    this->list = list;
    this->searchDir = searchDir;
    this->inpRegex = inpRegex;
    this->status = status;

    connect(btnSearch, &QPushButton::clicked, this, &SearchInteractor::search);
    connect(btnErase, &QPushButton::clicked, this, &SearchInteractor::erase);
    connect(list, &QListWidget::itemDoubleClicked, this, &SearchInteractor::listItemDoubleClicked);
}


void SearchInteractor::search()
{
    erase();

    QStringList* result = new QStringList();
    QRegExp regex(inpRegex->text());

    QProgressDialog progressDialog(list);

    progressDialog.setWindowTitle(tr("Пошук файлів"));
    progressDialog.setCancelButtonText(tr("Зупинити"));
    progressDialog.setLabelText(tr("Сканування..."));
    status->showMessage(tr("Сканування..."));
    progressDialog.show();

    findRecursive(QDir::cleanPath(searchDir->currentText()), result, progressDialog);

    progressDialog.setLabelText(tr("Триває пошук..."));
    progressDialog.setRange(0, result->size());

    for (int i = 0; i < result->size(); ++i) {
        progressDialog.setValue(i);
        QCoreApplication::processEvents();

        if (progressDialog.wasCanceled())
            break;

        status->showMessage(tr("Перевірка відповідності: ") + result->at(i));
        if (regex.exactMatch(result->at(i))) {
            list->addItem(new QListWidgetItem(result->at(i), list));
        }
    }
    status->showMessage(tr("Готово"));
}

void SearchInteractor::findRecursive(const QString &path, QStringList *result, QProgressDialog& progress)
{
    QDir currentDir(path);
    const QString prefix = path + QLatin1Char('/');
    foreach (const QString &match, currentDir.entryList(QDir::Files | QDir::NoSymLinks)){
        status->showMessage(tr("Сканування: ") + prefix + match);
        QCoreApplication::processEvents();
        if (progress.wasCanceled())
            break;
        result->append(prefix + match);
    }
    foreach (const QString &dir, currentDir.entryList(QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot))
        findRecursive(prefix + dir, result, progress);
}

void SearchInteractor::erase()
{
    list->clear();
}

void SearchInteractor::listItemDoubleClicked(QListWidgetItem *item)
{
    QString param;
    if (!QFileInfo(item->text()).isDir())
        param = QLatin1String("/select,");
    param += QDir::toNativeSeparators(item->text());
    QString command = "explorer.exe " + param;
    QProcess::startDetached(command);
}



